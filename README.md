# PowerShell and DevOps Summit 2017 Trip Report

* Bellevue, WA
* April 9-12, 2017
* Mike Lombardi

## What is PowerShell and DevOps Summit 2017

This summit was a chance to spend time with the top experts of both PowerShell and devops practices in Windows environments.
The focus at this summit is around 300+ level information and technical presentations.
It is also a chance to dicuss patterns and strategies for managing environments in the modern enterprise.

## Why I chose to attend PowerShell and DevOps Summit 2017

I wanted to attend this conference primarily to have access to the minds of the other attendees for a whole week.
I anticipated that the discussions I would have with these folks would help drive me towards better practices.

I _also_ wanted to see how people were implementing cutting-edge technologies for the Windows stack, especially from an automation or management perspective.

## Takeaways

+ PowerShell itself as well as the use of it is continuing to steer towards a community-focus.
+ Two things will be true for the foreseeable future:
  1. We will be operating in hybrid (public and private) cloud environments
  2. We will be supporting "legacy" applications in our environments
+ The greatest and most immediately useful value we can gain is from setting up a pipeline
  + We can't boil the ocean, but we _can_ get **something** delivered safely, repeatably, and consistently.
  + We should focus on getting this pipeline up and running first, then look at pulling in additional pieces to deliver from it.
  + _Iterate, don't increment!_
+ The mindset is much more important than the specific tools - CI/CD and automation still absolutely need to be implemented.
+ Testing can be more valuable than automation
+ We should begin to implement JEA - it's okay to start with a break-glass endpoint and add safer ones over time.
  + We're effectively using a less-safe version of the break-glass endpoint now!
+ SSH is coming and may be the preferred remote management protocol in a hybrid environment.
+ Apprenticeships might be an effective tool for building a better pipeline of IT professionals
  + Apparently there are tax benefits too


## Detailed Session Information

### DSC / Module Labs with Steven Murawski & Kirk Munro

#### Summary
Spent the session with two of the best and brightest minds talking about writing DSC resources and modules, as well as using them in production.

#### Takeaways
+ DSC is powerful and useful, but there's things it doesn't do well
  + For example, complex multilayered configurations where things change based on environment and role.
  + It doesn't do much for you with secrets management
  + The reporting and auditing mechanisms are weak
  + We're better off using DSC as a platform underneath Chef/Puppet
+ Module development maturity should be stressed at an organizational level
+ Re-implementing DSC resources if you can use existing ones is a terrible anti-pattern.

#### Action Items
+ [ ] Find an actual example of a DSC resource that needs writing in our environment, write it via PSv5.1 Classes.
+ [ ] Review our documentation and practices around module development, codify successful patterns and train the teams on them.

### State of the Community with Don Jones

#### Summary
A talk about where the community is at, how to get involved, and new projects rolling out.

#### Takeaways
+ The PowerShell community is maturing into a truly open-source-centric community
+ The DevOps collective is putting in work to make hosting PowerShell Saturdays easier
+ There's a mentorship program in the pipeline for getting highschool students into novice-level IT roles.
  + This could be particularly useful for us in building a pipeline of reliable, interested talent that shares our values and can broaden our teams.
+ The community needs more writers and maintainers to truly mature.

#### Action Items
+ [ ] Write a short "book" on _Documenting PowerShell Projects_
Probably base this on prior work I've started to do.
+ [ ] Enroll in the mentor program _as soon as possible_
+ [ ] Plan to host and execute a PowerShell Saturday either annually or twice-per-year.
Target February 2018 for the first run.

### Ask Me Anything with Jeffrey Snover

#### Summary
Chance to listen to the "Shellfather" discuss PowerShell and answer questions about technologies and patterns.

#### Takeaways
+ PowerShell has undergone some serious cultural changes in the last several years, but especially the last year, shifting _towards_ open source.
+ We should be implementing JEA and turning on transcripting.
+ 2FA already exists for PowerShell, we need to go forth and implement it.
+ OpenSSH + PowerShell + JEA is _the_ secure interactive management platform for our servers.

#### Action Items
+ [ ] Research 2FA for PowerShell
+ [ ] Look into a simple JEA endpoint setup
+ [ ] Turn on transcripting and see how much noise is generated.
  + [ ] Maybe use the transcripts to build smaller, more scoped JEA endpoints.

### State of the Shell with the PowerShell Team

#### Summary
Updates on where PowerShell is and where it's going over the next year.

#### Takeaways
+ Again, SSH on Windows / for PowerShell.
+ PowerShell on Linux.
+ DSC is improving substantially over the next year.
  + We may or may not be too interested in this as we roll forward with Chef/Puppet.

#### Action Items
+ [ ] Investigate PowerShell on Linux more closely.
  + [ ] Look for ways to reduce management action duplication

### Building a JEA Solution in Formula One with O'Neill

#### Summary
Discussion of actual implementation of JEA in an environment.

#### Takeaways
+ PowerShell commands can be made private and thus unseeable in the console.
+ Creating a proxy function is a useful way to limit commands to specific options
+ Restricted language modes can be used to limit non-command options (creating functions, for example)
+ Constrained endpoints can be used to setup an environment and execute as a particular user with the necessary credentials
  + This is an option for resolving the double-hop problem!
+ Remoting is, eventually, something of an anti-pattern - but only because we should have truly positive control over what goes into the environment.
  + Until that day, PowerShell remoting via secured, constrained endpoints with JEA and transcripting is the safest, sanest remote management tooling option.

#### Action Items
+ [ ] Research constrained endpoints, restricted language modes, and private functions.
+ [ ] Implement a basic proxy module for simple actions (such as service management).

### PowerShell Saturday with Don Jones

#### Summary
Discussion regarding running a PowerShell Saturday and the available tooling for making this easier.

#### Takeaways
+ Any planned event like this requires a business plan.
+ DevOps Collective is rendering an _awesome_ community service by offering this help.
+ Help includes: registration and finances, scheduling tools, polling tools, help with insurance if needed, and advice.
+ Start small, expand later.

#### Action Items
+ [ ] Host a PowerShell Saturday
  + [ ] Check with training centers, local chambers of commerce, universities, and hotels
  + [ ] Get a webpage
  + [ ] Include a (good) code of conduct
  + [ ] Reach out to local Microsoft training center for a mailing list of interested folks
  + [ ] Look into colocation with another event
  + [ ] Plan a no-charge after-conference event
  + [ ] Reach out for co-organizers and to previous organizers
  + [ ] Define clear vision of what and why we're putting on the conference
  + [ ] Reach out to local IT Directors
  + [ ] Consider an internal PS Saturday

### PKI: Not the Snoozefest You Expect with Januszco

#### Summary
It is entirely possible to generate and deploy an enterprise PKI setup from code and to maintain it the same way.

#### Takeaways
+ This is easier to setup and maintain than doing it by hand
+ DSC deployment methodology has come a long way since I last looked at it
+ Certificates management is still hard.

#### Action Items
+ [ ] Review the [source code for this project](https://github.com/majst32/SummitPKI) and see what practices/options can be adopted internally
+ [ ] Review environment with architects for other 'too hard' configuration tasks we might've overlooked

### Auto-Generated UX with Kirk Munro and David Wilson

#### Summary
Discussion and demo of a new project, [Phosphor](https://github.com/PowerShell/Phosphor), for generating a gui from PowerShell code.

#### Takeaways
+ This is 100% the best tool to turn over to our front-line folks
+ The tool is intended to be able to hook into JEA endpoints, meaning it can be used to provide a secure tooling for our helpdesk.
+ This reduces the cognitive load for onboarding people into using PowerShell instead of RDP
+ The tool is crossplatform and open source

#### Action Items
+ [ ] Try the project out locally
+ [ ] Work with the team to become a maintainer of the project (probably via documentation contribution)
+ [ ] Help provide user stories to the development team for use cases _we_ want to see in action.

### PowerShell Remoting and Kerberos Double-Hop with Ashley McGlone

#### Summary
Discussion of how to (securely, sanely) overcome the double-hop problem.

#### Takeaways
+ Couldn't attend due to fire marshal rules.

#### Action Items
+ [ ] Investigate and [review the resources](https://blogs.technet.microsoft.com/ashleymcglone/tag/kerberos/) that the presenter made available before I had to leave
+ [ ] See if this makes sense for us or if other methods of work around fit our context better.

### Debugging PowerShell with Kirk Munro

#### Summary
Discussion of how to debug PowerShell and think more like a developer for debugging.

#### Takeaways
+ I understand the terminology better now and can connect step into/step over with actual actions in the debugger.
+ I need to practice my debugging skills more thorougly so I can teach them to my organization.
+ Proper debugging is much faster than the methods I was pursuing before (execute with `Write-Host` for example)

#### Action Items
+ [ ] Debug a single intentionally broken function in VSCode using these techniques
+ [ ] Teach my team mates how to do the same.
+ [ ] Download and use the `DebugPX` and other `*PX` modules
+ [ ] Practice remote debugging
+ [ ] Write better error handling

### Community Lightning Demos

#### Summary
Numerous short (less than ten minute) demos by members of the community.
I talked about the [three types of documentation](http://michaeltlombardi.gitlab.io/needful-docs/) during my session.

#### Takeaways
+ These were some of the most interesting and valuable talks I saw - not directly, but because they gave me brief insights into the patterns and people executing real work in production.
+ Learned how to provide better intellisense
+ People are automating entire environment provisioning from CMDB
+ People are using chatops in production to excellent effect
  + Pushing changes, retrieving data and metrics, orchestrating fixes based on tickets, etc.
+ Testing needs to happen everywhere, all the time - not just our apps or nodes, but our infrastructure (VMWare and Cisco et al) too.
+ We can deploy infrastructure more safely and repeatably and quickly than ever before
  + Speed is a byproduct of safety and reliability
+ [Lability](https://github.com/VirtualEngine/Lability) is an incredible tool for spinning up dev environments and throwing them away after use.
+ People are using Jenkins to deploy and execute PowerShell to solve problems
  + More evidence that CI-class tools are important
+ We should be using package management for any artifacts we _can_ use package management for.
  + Safer, more repeatable, and much faster.
+ We need to reach out to more folks for talks like these; all of them were valuable!

#### Action Items
+ [ ] See if I can drum up more speakers for the next round of lightning talks
+ [ ] Encourage folks who've never spoken before to give a talk
+ [ ] Refactor a project to provide betterintellisense
+ [ ] Look into testing for things that aren't applications, PowerShell code, or windows Nodes.
+ [ ] Evangelize the value of pipelines and repeatability as well as patterns for getting there.